﻿using Movie_Characters_API.Models.Domain;

namespace Movie_Characters_API_DB.Services
{
	public interface ICharacterService : IService<Character>
	{
	}
}