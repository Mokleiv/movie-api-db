# movie-characters

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Assignment for utilising EF Core and create a Web API.

## Table of Contents

-   [Install](#install)
-   [Usage](#usage)
-   [API](#api)
-   [Maintainers](#maintainers)
-   [Contributing](#contributing)
-   [License](#license)

## Install

Open the solution in Visual Studio. Add servername to datasource in the file `appsettings.json`. Run these two commands in Nuget Package Manager Console:

```
add-migration Init-db
```

```
update-database
```

## Usage

```
Run in Visual Studio
```

## Database diagram

![Database diagram](./DatabaseDiagram.png "Database diagram")

## Maintainers

[@Mokleiv](https://github.com/@Mokleiv) [@mikaelb](https://gitlab.com/mikaelb)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Sondre Mokleiv Nygård
