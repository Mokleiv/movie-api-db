﻿using AutoMapper;
using Movie_Characters_API.Models.Domain;
using Movie_Characters_API_DB.Models.DTOs.Franchise;
using System.Linq;

namespace Movie_Characters_API_DB.Profiles
{
	public class FranchiseProfile : Profile
	{

		public FranchiseProfile()
		{
			CreateMap<Franchise, FranchiseReadDTO>()
				.ForMember(pdto => pdto.Movies, opt =>
				 opt.MapFrom(p => p.Movies.Select(s => s.Id).ToArray()))
				.ReverseMap();
			CreateMap<Franchise, FranchiseCreateDTO>()
				.ReverseMap();
			CreateMap<Franchise, FranchiseEditDTO>()
				.ReverseMap();
		}

	}
}
