﻿using AutoMapper;
using Movie_Characters_API.Models.Domain;
using Movie_Characters_API_DB.Models.DTOs.Movie;
using System.Linq;

namespace Movie_Characters_API_DB.Profiles
{
	public class MovieProfile : Profile
	{

		public MovieProfile()
		{
			CreateMap<Movie, MovieReadDTO>()
							.ForMember(pdto => pdto.Characters, opt =>
							 opt.MapFrom(p => p.Characters.Select(s => s.Id).ToArray()))
							.ReverseMap();
			CreateMap<Movie, MovieCreateDTO>()
				.ReverseMap();
			CreateMap<Movie, MovieEditDTO>()
				.ReverseMap();
		}

	}
}
