﻿using AutoMapper;
using Movie_Characters_API.Models.Domain;
using Movie_Characters_API_DB.Models.DTOs.Character;
using System.Linq;

namespace Movie_Characters_API_DB.Profiles
{
	public class CharacterProfile : Profile
	{

		public CharacterProfile()
		{
			CreateMap<Character, CharacterReadDTO>()
				.ForMember(pdto => pdto.Movies, opt =>
				 opt.MapFrom(p => p.Movies.Select(s => s.Id).ToArray()))
				.ReverseMap();
			CreateMap<Character, CharacterCreateDTO>()
				.ReverseMap();
			CreateMap<Character, CharacterEditDTO>()
				.ReverseMap();
		}
	}
}
