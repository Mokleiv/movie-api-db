﻿using Microsoft.EntityFrameworkCore;
using Movie_Characters_API.Models.Domain;

namespace Movie_Characters_API.Models.Data
{
	public class MovieDbContext : DbContext
	{
		public MovieDbContext(DbContextOptions options) : base(options)
		{
		}

		public DbSet<Character> Characters { get; set; }
		public DbSet<Movie> Movies { get; set; }
		public DbSet<Franchise> Franchises { get; set; }



		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Movie>().HasOne(m => m.Franchise).WithMany(f => f.Movies).OnDelete(DeleteBehavior.SetNull);
			modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Aragorn" });
			modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FullName = "Hawkeye" });
			modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FullName = "Batman" });
			modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Lord of the Rings" });
			modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "Marvel Cinematic Universe" });
			modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "DC Extended Universe" });
			modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, Title = "Lord of the Rings: The Two Towers", FranchiseId = 1 });
			modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, Title = "Avengers", FranchiseId = 2 });
			modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, Title = "Justice League: Snyder Cut", FranchiseId = 3 });
			modelBuilder.Entity<Character>()
				.HasMany(c => c.Movies)
				.WithMany(m => m.Characters)
				.UsingEntity(cm => cm.HasData(new { CharactersId = 1, MoviesId = 1 }));
			modelBuilder.Entity<Character>()
				.HasMany(c => c.Movies)
				.WithMany(m => m.Characters)
				.UsingEntity(cm => cm.HasData(new { CharactersId = 2, MoviesId = 2 }));
			modelBuilder.Entity<Character>()
				.HasMany(c => c.Movies)
				.WithMany(m => m.Characters)
				.UsingEntity(cm => cm.HasData(new { CharactersId = 3, MoviesId = 3 }));
		}
	}
}
