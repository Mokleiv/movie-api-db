﻿using System.Collections.Generic;

namespace Movie_Characters_API_DB.Models.DTOs.Franchise
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<int> Movies { get; set; }
    }
}
