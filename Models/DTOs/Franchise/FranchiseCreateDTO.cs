﻿namespace Movie_Characters_API_DB.Models.DTOs.Franchise
{
    public class FranchiseCreateDTO
    {
		public string Name { get; set; }
		public string Description { get; set; }
	}
}
