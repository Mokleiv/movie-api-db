using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Movie_Characters_API.Models.Data;
using Movie_Characters_API_DB.Services;
using System;
using System.IO;
using System.Reflection;





namespace Movie_Characters_API_DB
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			services.AddControllers();
			services.AddAutoMapper(typeof(Startup));
			services.AddScoped(typeof(IMovieService), typeof(MovieService));
			services.AddScoped(typeof(IFranchiseService), typeof(FranchiseService));
			services.AddScoped(typeof(ICharacterService), typeof(CharacterService));
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "Movie_Characters_API_DB v1",
					Version = "v1",
					Description = "ASP.NET Core Web API to do CRUD actions against the Movie DB",
					Contact = new OpenApiContact
					{
						Name = "Sondre Nyg�rd & Mikael Bjerga",
						Email = String.Empty,
					}
				});
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);

			});
			services.AddDbContext<MovieDbContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Movie_Characters_API_DB v1"));
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
